//
//  Utilities.swift
//  Bus
//
//  Created by Hesham El-Sawaf on 8/20/16.
//  Copyright © 2016 Abdellah. All rights reserved.
//

import Foundation
import MapKit

private let REGION_RADIUS: CLLocationDistance = 10000

func showSimpleAlertWithTitle(title: String!, message: String, viewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
    alert.addAction(action)
    viewController.presentViewController(alert, animated: true, completion: nil)
}

func zoomToUserLocationInMapView(mapView: MKMapView) {
    if let coordinate = mapView.userLocation.location?.coordinate {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, REGION_RADIUS, REGION_RADIUS)
        mapView.setRegion(region, animated: true)
    }
}
