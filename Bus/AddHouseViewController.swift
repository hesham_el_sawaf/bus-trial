//
//  addTargetController.swift
//  Bus
//
//  Created by Ahmed on 8/17/16.
//  Copyright © 2016 Abdellah. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


protocol AddHouseViewControllerDelegate {
    func addHouseViewController(controller: AddHouseViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                        radius: Double, identifier: String, note: String, eventType: EventType)
}

class AddHouseViewController : UITableViewController {
    
    @IBOutlet var addButton: UIBarButtonItem!
    @IBOutlet var zoomButton: UIBarButtonItem!
    
    @IBOutlet weak var eventTypeSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var radiusTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    
    var delegate: AddHouseViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItems = [self.addButton, self.zoomButton]
        self.addButton.enabled = false
        
        self.tableView.tableFooterView = UIView()
        self.map.showsUserLocation = true
    }
    
    @IBAction func textFieldEditingChanged(sender: UITextField) {
        self.addButton.enabled = !radiusTextField.text!.isEmpty && !noteTextField.text!.isEmpty
    }
    
    @IBAction func onCancel(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func onAdd(sender: UIBarButtonItem) {
        let coordinate = self.map.centerCoordinate
        let radius = (radiusTextField.text! as NSString).doubleValue
        let identifier = NSUUID().UUIDString
        let note = noteTextField.text!
        let eventType = (eventTypeSegmentedControl.selectedSegmentIndex == 0) ? EventType.OnEntry : EventType.OnExit
        self.delegate.addHouseViewController(self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, note: note, eventType: eventType)
    }
    
    @IBAction func onZoomToCurrentLocation(sender: UIBarButtonItem) {
        zoomToUserLocationInMapView(self.map)
    }
    
    
    
}
