//
//  Target.swift
//  Bus
//
//  Created by Ahmed on 8/17/16.
//  Copyright © 2016 Abdellah. All rights reserved.
//

import UIKit
import MapKit


private let houseLatitudeKey = "latitude"
private let houseLongitudeKey = "longitude"
private let houseRadiusKey = "radius"
private let houseIdentifierKey = "identifier"
private let houseNoteKey = "note"
private let houseEventTypeKey = "eventType"

enum EventType: Int {
    case OnEntry = 0
    case OnExit
}

class House: NSObject, NSCoding, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    private var radius: CLLocationDistance
    private var identifier: String
    private var note: String
    private var eventType: EventType
    
    var title: String? {
        if note.isEmpty {
            return "No Note"
        }
        return note
    }
    
    var subtitle: String? {
        let eventTypeString = eventType == .OnEntry ? "On Entry" : "On Exit"
        return "Radius: \(radius)m - \(eventTypeString)"
    }
    
    init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, identifier: String, note: String, eventType: EventType) {
        self.coordinate = coordinate
        self.radius = radius
        self.identifier = identifier
        self.note = note
        self.eventType = eventType
    }
    
    required init?(coder decoder: NSCoder) {
        let latitude = decoder.decodeDoubleForKey(houseLatitudeKey)
        let longitude = decoder.decodeDoubleForKey(houseLongitudeKey)
        self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.radius = decoder.decodeDoubleForKey(houseRadiusKey)
        self.identifier = decoder.decodeObjectForKey(houseIdentifierKey) as! String
        self.note = decoder.decodeObjectForKey(houseNoteKey) as! String
        self.eventType = EventType(rawValue: decoder.decodeIntegerForKey(houseEventTypeKey))!
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeDouble(coordinate.latitude, forKey: houseLatitudeKey)
        coder.encodeDouble(coordinate.longitude, forKey: houseLongitudeKey)
        coder.encodeDouble(radius, forKey: houseRadiusKey)
        coder.encodeObject(identifier, forKey: houseIdentifierKey)
        coder.encodeObject(note, forKey: houseNoteKey)
        coder.encodeInt(Int32(eventType.rawValue), forKey: houseEventTypeKey)
    }
    
    func getRadius() -> CLLocationDistance {
        return self.radius
    }
    
    func getIdentifier() -> String {
        return self.identifier
    }
    
    func getEventType() -> EventType {
        return self.eventType
    }
    
    func getNote() -> String {
        return self.note
    }
}
