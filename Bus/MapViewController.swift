//
//  mapViewController.swift
//  Bus
//
//  Created by Ahmed on 8/17/16.
//  Copyright © 2016 Abdellah. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private let savedHousesKey = "savedHouses"

class MapViewController: UIViewController  , MKMapViewDelegate ,AddHouseViewControllerDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet private weak var map: MKMapView!
    
    private var houses = [House]()
    private let locationManager = CLLocationManager()
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        self.map.showsUserLocation = (status == .AuthorizedAlways)
    }
    
    func locationManager(manager: CLLocationManager, monitoringDidFailForRegion region: CLRegion?, withError error: NSError) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.map.delegate = self
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.loadAllHouses()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "addHouse" {
            let navigationController = segue.destinationViewController as! UINavigationController
            let vc = navigationController.viewControllers.first as! AddHouseViewController
            vc.delegate = self
        }
    }
    
    private func loadAllHouses() {
        
        self.houses = []
        
        if let savedHouses = NSUserDefaults.standardUserDefaults().arrayForKey(savedHousesKey) {
            for savedHouse in savedHouses {
                if let houseInMap = NSKeyedUnarchiver.unarchiveObjectWithData(savedHouse as! NSData) as? House {
                    self.addHouse(houseInMap)
                }
            }
        }
    }
    
    private func saveAllHouses() {
        
        let houses = NSMutableArray()
        
        for house in self.houses {
            let houseInMap = NSKeyedArchiver.archivedDataWithRootObject(house)
            houses.addObject(houseInMap)
        }
        NSUserDefaults.standardUserDefaults().setObject(houses, forKey: savedHousesKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    private func addHouse(house: House) {
        self.houses.append(house)
        self.map.addAnnotation(house)
        self.addRadiusOverlayForHouse(house)
        self.updateHousesCount()
    }
    
    private func removeHouse(house: House) {
        if let indexInArray = self.houses.indexOf(house) {
            self.houses.removeAtIndex(indexInArray)
        }
        
        self.map.removeAnnotation(house)
        self.removeRadiusOverlayForHouse(house)
        self.updateHousesCount()
    }
    
    private func updateHousesCount() {
        title = "Houses (\(self.houses.count))"
        self.navigationItem.rightBarButtonItem?.enabled = (self.houses.count < 20)
    }
    
    
    
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myHouse"
        if annotation is House {
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .Custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteHouse")!, forState: .Normal)
                annotationView?.leftCalloutAccessoryView = removeButton
                annotationView?.animatesDrop = true
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.lineWidth = 1.0
        circleRenderer.strokeColor = UIColor.purpleColor()
        circleRenderer.fillColor = UIColor.purpleColor().colorWithAlphaComponent(0.4)
        return circleRenderer
        
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let house = view.annotation as! House
        self.stopMonitoringHouse(house)
        self.removeHouse(house)
        self.saveAllHouses()
    }
    
    private func addRadiusOverlayForHouse(house: House) {
        self.map.addOverlay(MKCircle(centerCoordinate: house.coordinate, radius: house.getRadius()))
    }
    
    private func removeRadiusOverlayForHouse(house: House) {
        let overlays = self.map.overlays
        
        for overlay in overlays {
            if let circleOverlay = overlay as? MKCircle {
                let coord = circleOverlay.coordinate
                if coord.latitude == house.coordinate.latitude && coord.longitude == house.coordinate.longitude && circleOverlay.radius == house.getRadius() {
                    self.map.removeOverlay(circleOverlay)
                    break
                }
            }
        }
    }
    
    
    @IBAction func zoomToCurrentLocation(sender: UIBarButtonItem) {
        zoomToUserLocationInMapView(self.map)
    }
    
    func addHouseViewController(controller: AddHouseViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)

        let clampedRadius = (radius > self.locationManager.maximumRegionMonitoringDistance) ? self.locationManager.maximumRegionMonitoringDistance : radius
        
        let house = House(coordinate: coordinate, radius: clampedRadius, identifier: identifier, note: note, eventType: eventType)
        self.addHouse(house)
        self.startMonitoringHouse(house)
        self.saveAllHouses()
    }
    
    
    private func regionWithHouse(house: House) -> CLCircularRegion {
        
        let region = CLCircularRegion(center: house.coordinate, radius: house.getRadius(), identifier: house.getIdentifier())
        
        region.notifyOnEntry = (house.getEventType() == .OnEntry)
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    private func startMonitoringHouse(house: House) {
        
        if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
            showSimpleAlertWithTitle("Error", message: "Monitoring is not supported on this device!", viewController: self)
            return
        }
        
        if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
            showSimpleAlertWithTitle("Warning", message: "Your house is saved but will only be activated once you grant Bus permission to access the device location.", viewController: self)
        }
        
        let region = self.regionWithHouse(house)
        
        self.locationManager.startMonitoringForRegion(region)
    }
    
    private func stopMonitoringHouse(house: House) {
        for region in self.locationManager.monitoredRegions {
            if let circularRegion = region as? CLCircularRegion {
                if circularRegion.identifier == house.getIdentifier() {
                    self.locationManager.stopMonitoringForRegion(circularRegion)
                }
            }
        }
    }
    
}